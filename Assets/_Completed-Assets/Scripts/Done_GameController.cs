﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class Done_GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public GUIText scoreText;
    public GUIText restartText;
    public GUIText gameOverText;

    private bool gameOver;
    private bool restart;
    private int score;
    GameEventRecoder gameEventRecoder = new GameEventRecoder();

    void Start()
    {
        gameEventRecoder.Init();
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        UpdateScore();
        StartCoroutine(SpawnWaves());

    }

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
				gameObject.GetComponent<InterstitialAdsScript>().ShowInterstitialAd();
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
		gameEventRecoder.MilestoneEvent(score);
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    public void GameOver(string killer)
    {
        gameEventRecoder.GameOverEvent(killer, score);
        gameOverText.text = "Game Over!";
        gameOver = true;
    }
}
class GameEventRecoder : IEventRecoder
{
    bool AchieveScore_1000,AchieveScore_500,AchieveScore_250,AchieveScore_100;
    string killer;
    public void Init()
    {
        killer = "";
        AchieveScore_1000 = false;
        AchieveScore_500 = false;
        AchieveScore_250 = false; 
        AchieveScore_100 = false;
    }
    public void GameOverEvent(string killerName,int score)
    {
		if(killer == "")
		{
			killer = killerName;
			Debug.Log("killerName: " + killer);
			AnalyticsEvent.GameOver("GameOver", new Dictionary<string, object>
			{
				{ "KillerName: ", killer},
				{ "Score: ", score}
			});
		}
    }

    public void MilestoneEvent(int score)
    {
		if(score >= 1000 && !AchieveScore_1000)
		{
			AnalyticsEvent.AchievementStep(4, "Achieve Score:1000 Milestone", new Dictionary<string, object>
			{
				{"Milestone at 1000: ", score}
			});
			Debug.Log("Achieve Score:1000 Milestone: " + score);
			AchieveScore_1000 = true;
		}
		else if(score >= 500 && !AchieveScore_500)
		{
			AnalyticsEvent.AchievementStep(3, "Achieve Score:500 Milestone", new Dictionary<string, object>
			{
				{"Milestone at 500: ", score}
			});
			Debug.Log("Achieve Score:500 Milestone: " + score);
			AchieveScore_500 = true;
		}
		else if(score >=250 && !AchieveScore_250)
		{
			AnalyticsEvent.AchievementStep(2, "Achieve Score:250 Milestone", new Dictionary<string, object>
			{
				{"Milestone at 250: ", score}
			});
			Debug.Log("Achieve Score:250 Milestone: " + score);
			AchieveScore_250 = true;
		}
		else if(score >= 100 && !AchieveScore_100)
		{
			AnalyticsEvent.AchievementStep(1, "Achieve Score:100 Milestone", new Dictionary<string, object>
			{
				{"Milestone at 100: ", score}
			});
			Debug.Log("Achieve Score:100 Milestone: " + score);
			AchieveScore_100 = true;
		}
    }

}
